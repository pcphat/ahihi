<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Task;

// Route::get('/', function () {
//     return view('welcome', ['name' => 'Phan Cong Phat']);
// });

Route::get('/', 'PostsController@index');
Route::get('/posts/{post}', 'PostsController@show');

//--------------------TASK--------------------

Route::get('/tasks', 'TasksController@index');
Route::get('/tasks/{task}','TasksController@show');

//------------------END TASK------------------


Route::get('about', function () {

    $tasks = [
        "ahihi",
        "ahoho",
        "ahuhu"
    ];
    return view('about', compact('tasks'));
});
